export class Model {
	user;
	items;

	constructor() {
		this.user = "thekyz";
		this.items = [
			new TodoItem("do stuff", false),
		  new TodoItem("do other stuff", true),
		  new TodoItem("do something", false)
		]
	}
};

export class TodoItem {
	action;
	done;

	constructor(action, done) {
		this.action = action;
		this.done = done;
	}
};
